﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VisualStudio_App;
using SQLite;
using System;

namespace VisualStudio_App
{
    public class Database
    {
        readonly SQLiteAsyncConnection _database;

        public Database(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Person>().Wait();
            _database.CreateTableAsync<Note>().Wait();
            _database.CreateTableAsync<User>().Wait();
        }

        public Task<List<Person>> GetPeopleAsync()
        {
            return _database.Table<Person>().ToListAsync();
        }
        public Task<List<User>> GetUsersAsync()
        {
            return _database.Table<User>().ToListAsync();
        }

        public Task<List<Note>> GetNotesAsync()
        {
            return _database.Table<Note>().ToListAsync();
        }
        public Task<List<Note>> GetNotesByUserIdAsync(int userId)
        {
            return _database.Table<Note>().Where(i => i.UserId == userId).ToListAsync();
        }
        public Task<int> SavePersonAsync(Person person)
        {
            return _database.InsertAsync(person);
        }
        public Task<int> SaveUserAsync(User user)
        {
            return _database.InsertAsync(user);
        }        
        public Task<int> SaveNoteAsync(Note note)
        {
            return _database.InsertAsync(note);
        }        
        public Task<int> UpdateNoteAsync(Note note)
        {
            return _database.UpdateAsync(note);
        }
        public Task<int> UpdateUserAsync(User user)
        {
            return _database.UpdateAsync(user);
        }
        public Task<Person> GetPersonAsync(int id)
        {
            return _database.Table<Person>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }
        public Task<User> GetUserAsync(int id)
        {
            return _database.Table<User>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }        
        async public Task<Note> GetNoteAsync(int id)
        {
            return await _database.Table<Note>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }
        public Task<int> DeletePersonAsync(Person person)
        {
            return _database.DeleteAsync(person);
        }
        public Task<int> DeleteUserAsync(User user)
        {
            return _database.DeleteAsync(user);
        }
        public Task<int> DeleteNoteAsync(Note note)
        {
            return _database.DeleteAsync(note);
        }

        public Task<Note> GetNoteFromDate(DateTime date)
        {
            return _database.Table<Note>().Where(i => i.DateBegin == date ).FirstOrDefaultAsync();
        }
        public Task<User> GetUserFromPseudo(string pseudo)
        {
            return _database.Table<User>().Where(i => i.pseudo == pseudo).FirstOrDefaultAsync();
        }        
        async public Task<List<Note>> GetNotesFromDate(DateTime date, int userId)
        {
            List<Note> listNotes = await _database.Table<Note>().Where(i => i.UserId == userId).ToListAsync();
            List<Note> finalList = new List<Note>();
            foreach(Note note in listNotes)
            {
                if(note.DateBegin.Year <= date.Year && note.DateBegin.Month <= date.Month && note.DateBegin.Day <= date.Day && note.DateEnd.Year >= date.Year && note.DateEnd.Month >= date.Month && note.DateEnd.Day >= date.Day)
                {
                    finalList.Add(note);
                }
            }
            return finalList;
        }

        async public Task<string> AddUser(User user)
        {
            var data = _database.Table<User>();
            var d1 = await data.Where(x => x.pseudo == user.pseudo).FirstOrDefaultAsync();
            if (d1 == null)
            {
                await _database.InsertAsync(user);
                return "Ajouté avec succès.";
            }
            else return "Le pseudo existe déjà.";
        }
        async public Task<bool> updateUserValidation(string pseudo) 
        {
            var d1 = await _database.Table<User>().Where(i => i.pseudo == pseudo).CountAsync();
            if (d1 == 1)
                return true;
            else
                return false;
        } 
        async public Task<bool> LoginValidate(string pseudo,string pswd) 
        {
            var d1 = await _database.Table<User>().Where(i => i.pseudo == pseudo && i.password == pswd).CountAsync();
            if (d1 >= 1)
                return true;
            else
                return false;
        }
    }
}
