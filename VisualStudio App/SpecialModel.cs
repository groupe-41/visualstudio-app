﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using XamForms.Controls;

namespace VisualStudio_App
{
    public class SpecialModel 
    {
        public string Title { set; get; } 
        public DateTime Date { set; get; }    
        public Calendar Calendar { set; get; }

//Ajouter dates des notes, et sortir une liste
        public void AddSpecialDateWithList(List<SpecialModel> list)
    {
        List<SpecialDate> newList = new List<SpecialDate>();
        foreach (SpecialModel model in list)
        {
            SpecialDate newDate = new SpecialDate(model.Date)
            {
                Selectable = true,
                BackgroundPattern = new BackgroundPattern(1)
                {
                    Pattern = new List<Pattern>
                         {
                             new Pattern { WidthPercent = 1f, HightPercent = 0.6f, Color = Color.Transparent },
                             new Pattern { WidthPercent = 1f, HightPercent = 0.4f, Color = Color.Transparent, Text = model.Title, TextColor=Color.Black, TextSize=11, TextAlign=TextAlign.Middle },
                         }
                }
            };

            newList.Add(newDate);
        }
        Calendar.SpecialDates = newList;
    }
    }
}
