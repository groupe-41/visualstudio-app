﻿using VisualStudio_App;
using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VisualStudio_App
{
    public partial class App : Application
    {
        static Database database;

        public static Database Database
        {
            get
            {
                if (database == null)
                {
                    database = new Database(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "people.db3"));
                }
                return database;
            }
        }

        public static int userId { get; set; }

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage()) 
            { 
                BarBackgroundColor = Color.CornflowerBlue,
            };
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
