﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

public partial class MainPage : ContentPage
{
    Button logInButton;
    Label labelOr;
    Button signInButton;

    public MainPage()
    {
        NavigationPage.SetHasNavigationBar(this, false);
        this.Padding = new Thickness(30, 30, 30, 30);
        StackLayout panel = new StackLayout
        {
            IsVisible = true,
            VerticalOptions = LayoutOptions.CenterAndExpand,
        };
        panel.Children.Add(logInButton = new Button
        {
            FontSize = 15,
            BorderColor = Color.FromHex("#000000"),
            CornerRadius = 5,
            Text = "Se connecter",
            TextColor = Color.FromHex("#ffffff"),
            BackgroundColor = Color.FromHex("#3399ff"),
    });
        panel.Children.Add(labelOr = new Label
        {
            Text = "ou",
            HorizontalOptions = LayoutOptions.Center
        });
        panel.Children.Add(signInButton = new Button
        {
            FontSize = 15,
            BorderColor = Color.FromHex("#000000"),
            CornerRadius = 5,
            Text = "S'inscrire",
            TextColor = Color.FromHex("#ffffff"),
            BackgroundColor = Color.FromHex("#00b33c"),
        });

        logInButton.Clicked += LoginButton_Clicked;
        signInButton.Clicked += SignUp_Clicked;

        this.Content = panel;


    }
    private async void LoginButton_Clicked(object sender, EventArgs e)

    {

        await Navigation.PushAsync(new LoginPage());

    }

    private async void SignUp_Clicked(object sender, EventArgs e)

    {

        await Navigation.PushAsync(new RegisterPage());

    }
}
