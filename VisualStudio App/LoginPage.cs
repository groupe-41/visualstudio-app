﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using VisualStudio_App;
using Xamarin.Essentials;
using Xamarin.Forms;

public partial class LoginPage : ContentPage
{
    ContentView mainView;
    Label connectLabel;
    Entry pseudoEntry;
    Entry passwordEntry;
    Button logInButton;
    Label forgotLabel;
    ContentView userVerifView;
    Label enterLabel;
    Entry userValidationEntry;
    Button validateUserButton;
    Button cancelButton;
    ContentView passwordView;
    Label passwordLabel;
    Grid verifGrid;
    Entry firstPasswordEntry;
    Entry secondPasswordEntry;
    Label warningLabel;
    Button submitButton;
    Button cancel2Button;
    Grid submitGrid;
    StackLayout panel;
    StackLayout part1;
    StackLayout part2;
    StackLayout part3;
    string logesh;
    public LoginPage()
    {
        NavigationPage.SetHasNavigationBar(this, false);

        this.Padding = new Thickness(30, 30, 30, 30);

        panel = new StackLayout
        {
            IsVisible = true,
            VerticalOptions = LayoutOptions.CenterAndExpand,
        };
        part1 = new StackLayout
        {
            IsVisible = true,
        };
        part1.Children.Add(connectLabel = new Label
        {
            TextColor = Color.Black,
            FontAttributes = FontAttributes.Bold,
            Text = "Connexion",
            FontSize = 20,
        });
        part1.Children.Add(pseudoEntry = new Entry
        {
            ReturnType = ReturnType.Next,
            Placeholder = "Pseudo",
            FontSize = 15,
            TextColor = Color.Black
        });
        part1.Children.Add(passwordEntry = new Entry
        {
            ReturnType = ReturnType.Done,
            IsPassword = true,
            Placeholder = "Mot de passe",
            FontSize = 15,
            TextColor = Color.Black
        });
        part1.Children.Add(logInButton = new Button
        {
            FontSize = 15,
            TextColor = Color.White,
            BorderColor = Color.Black,
            BackgroundColor = Color.FromHex("#00b33c"),
            HorizontalOptions = LayoutOptions.CenterAndExpand,
            WidthRequest = DeviceDisplay.MainDisplayInfo.Width,
            CornerRadius = 5,
            Text = "Se connecter"
        });
        part1.Children.Add(forgotLabel = new Label
        {
            Text = "Mot de passe oublié",
            TextColor = Color.Blue,
            FontSize = 15,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
            BackgroundColor = Color.Transparent
        });
        part2 = new StackLayout
        {
            IsVisible = true,
            VerticalOptions = LayoutOptions.CenterAndExpand,
            Padding = 5,
        };
        part2.Children.Add(enterLabel = new Label
        {
            Text = "Mot de passe oublié",
            VerticalOptions = LayoutOptions.StartAndExpand,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
        });
        part2.Children.Add(userValidationEntry = new Entry
        {
            Keyboard = Keyboard.Email,
            Placeholder = "Entrez votre Pseudo",
            TextColor = Color.Black,
        });
        part2.Children.Add(verifGrid = new Grid
        {
            ColumnDefinitions = { new ColumnDefinition(), new ColumnDefinition() }
        });
        verifGrid.Children.Add(validateUserButton = new Button
        {
            FontSize = 15,
            TextColor = Color.White,
            BorderColor = Color.Black,
            BackgroundColor = Color.Blue,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
            WidthRequest = DeviceDisplay.MainDisplayInfo.Width,
            CornerRadius = 5,
            Text = "Vérifier"

        },0,0);
        verifGrid.Children.Add(cancelButton = new Button
        {
            FontSize = 15,
            TextColor = Color.White,
            BorderColor = Color.Black,
            BackgroundColor = Color.Red,
            Text = "Annuler",
            HorizontalOptions = LayoutOptions.CenterAndExpand,
            WidthRequest = DeviceDisplay.MainDisplayInfo.Width,
            CornerRadius = 5,
        },1,0);
        part3 = new StackLayout
        {
            IsVisible = true,
            VerticalOptions = LayoutOptions.CenterAndExpand,
            Padding = 5
        };
        part3.Children.Add(passwordLabel = new Label
        {
            Text = "Mot de passe oublié",
            VerticalOptions = LayoutOptions.StartAndExpand,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
        });
        part3.Children.Add(firstPasswordEntry = new Entry
        {
            ReturnType = ReturnType.Next,
            Keyboard = Keyboard.Email,
            IsPassword = true,
            Placeholder = "Entrez votre mot nouveau de passe",
            TextColor = Color.Black,
            VerticalOptions = LayoutOptions.FillAndExpand,
            HorizontalOptions = LayoutOptions.FillAndExpand
        });
        part3.Children.Add(secondPasswordEntry = new Entry
        {
            ReturnType = ReturnType.Done,
            Keyboard = Keyboard.Email,
            IsPassword = true,
            Placeholder = "Confirmez votre nouveau mot de passe",
            TextColor = Color.Black,
            VerticalOptions = LayoutOptions.FillAndExpand,
            HorizontalOptions = LayoutOptions.FillAndExpand
        });
        part3.Children.Add(warningLabel = new Label
        {
            IsVisible = false,
            TextColor = Color.Blue,
            FontSize = 15,
            BackgroundColor = Color.Transparent,
        });
        part3.Children.Add(submitGrid = new Grid
        {
            ColumnDefinitions = { new ColumnDefinition(), new ColumnDefinition() }
        });
        submitGrid.Children.Add(submitButton = new Button 
        {
            FontSize = 15,
            TextColor = Color.White,
            BorderColor = Color.Black,
            BackgroundColor = Color.Green,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
            WidthRequest = DeviceDisplay.MainDisplayInfo.Width,
            CornerRadius = 5,
            Text = "Mettre à jour",
        });
        submitGrid.Children.Add(cancel2Button = new Button
        {
            FontSize = 15,
            TextColor = Color.White,
            BorderColor = Color.Black,
            BackgroundColor = Color.Red,
            Text = "Annuler",
            HorizontalOptions = LayoutOptions.CenterAndExpand,
            WidthRequest = DeviceDisplay.MainDisplayInfo.Width,
            CornerRadius = 5,
        }, 1, 0);
        panel.Children.Add(mainView = new ContentView {
            BackgroundColor = Color.Transparent,
            IsVisible = true,
            Content = part1
        });
        panel.Children.Add(userVerifView = new ContentView
        {
            BackgroundColor = Color.White,
            Padding = new Thickness(0,0),
            IsVisible = false,
            Content = part2
        });
        panel.Children.Add(passwordView = new ContentView
        {
            BackgroundColor = Color.White,
            Padding = new Thickness(0, 0),
            IsVisible = false,
            Content = part3
        });

        pseudoEntry.ReturnCommand = new Command(() => passwordEntry.Focus());
        firstPasswordEntry.ReturnCommand = new Command(() => secondPasswordEntry.Focus());
        var forgetpassword_tap = new TapGestureRecognizer();
        forgetpassword_tap.Tapped += Forgetpassword_tap_Tapped;
        forgotLabel.GestureRecognizers.Add(forgetpassword_tap);
        cancelButton.Clicked += OnCancelClicked;
        cancel2Button.Clicked += OnCancelClicked;
        logInButton.Clicked += LoginValidation_ButtonClicked;
        validateUserButton.Clicked += userIdCheckEvent;
        submitButton.Clicked += Password_ClickedEvent;

        this.Content = panel;

    }
    private void Forgetpassword_tap_Tapped(object sender, EventArgs e)
    {
        BackgroundChange(true);        
    }

    private async void userIdCheckEvent(object sender, EventArgs e)
    {
        if ((string.IsNullOrWhiteSpace(userValidationEntry.Text)) || (string.IsNullOrWhiteSpace(userValidationEntry.Text)))
        {
            await DisplayAlert("Alert", "Entrez votre Pseudo", "OK");
        }
        else
        {
            logesh = userValidationEntry.Text;
            var textresult = await App.Database.updateUserValidation(userValidationEntry.Text);
            if (textresult == true)
            {
                userVerifView.IsVisible = false;
                passwordView.IsVisible = true;
            }
            else
            {
                await DisplayAlert("Ce Pseudo n'existe pas", "Entrez un Pseudo existant", "OK");
            }
        }
    }

    private async void Password_ClickedEvent(object sender, EventArgs e)
    {
        if (!string.Equals(firstPasswordEntry.Text, secondPasswordEntry.Text))
        {
            warningLabel.Text = "Entrez un mot de passe similaire";
            warningLabel.TextColor = Color.IndianRed;
            warningLabel.IsVisible = true;
        }
        else if ((string.IsNullOrWhiteSpace(firstPasswordEntry.Text)) || (string.IsNullOrWhiteSpace(secondPasswordEntry.Text)))
        {
            await DisplayAlert("Alert", " Entrez votre mot de passe", "OK");
        }
        else
        {
            try
            {
                User user = await App.Database.GetUserFromPseudo(logesh);
                user.password = firstPasswordEntry.Text;
                var return1 = await App.Database.UpdateUserAsync(user);
                BackgroundChange(false);
                if (return1 >= 1)
                {
                    await DisplayAlert("Mot de passe mis à jour", "Profil mis à jour", "OK");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }       
    }
    private async void LoginValidation_ButtonClicked(object sender, EventArgs e)
    {
        if (pseudoEntry.Text != null && passwordEntry.Text != null)
        {
            var validData = await App.Database.LoginValidate(pseudoEntry.Text, passwordEntry.Text);
            if (validData == true)
            {
                BackgroundChange(false);
                User user = await App.Database.GetUserFromPseudo(pseudoEntry.Text);
                App.userId = user.Id;
                await Navigation.PushAsync(new NoteList());
            }
            else
            {
                BackgroundChange(false);
                await DisplayAlert("Erreur d'identification", "Pseudo ou mot de passe incorrect", "OK");
            }
        }
        else
        {
            BackgroundChange(false);
            await DisplayAlert("He He", "Entrez votre Pseudo et mot de passe", "OK");
        }
    }
    private async void BackgroundChange(bool state)
    {
        if(state == true)
        {           
            await this.ColorTo(Color.White, Color.FromHex("#ddd"), c => BackgroundColor = c, 200);
            userVerifView.IsVisible = true;
            mainView.IsVisible = false;
            passwordView.IsVisible = false;
        }
        else
        {
            await this.ColorTo(Color.FromHex("#ddd"),Color.White, c => BackgroundColor = c, 200);
            mainView.IsVisible = true;
            userVerifView.IsVisible = false;
            passwordView.IsVisible = false;
        }
    }
    private void OnCancelClicked(object sender, EventArgs e)
    {
        BackgroundChange(false);
    }
}
