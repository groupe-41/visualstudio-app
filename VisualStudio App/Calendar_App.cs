﻿using System;
using System.Collections.Generic;
using System.Text;
using VisualStudio_App;
using Xamarin.Forms;
using XamForms.Controls;


public partial class Calendar_App : ContentPage
{
    Calendar cal;
    Label label;
    ListView listView;
    int countRow = 0;
    public Calendar_App()
    {
        this.Padding = new Thickness(0, 20, 0, 20);
        StackLayout diary = new StackLayout
        {
            Spacing = 15
        };

        diary.Children.Add(label = new Label { 
           Text = "Agenda",
           FontAttributes = FontAttributes.Bold,
           HorizontalOptions = LayoutOptions.Center,
           FontSize = 18
        });
        diary.Children.Add(cal = new Calendar
        {
            BorderColor = Color.AliceBlue,
            BorderWidth = 3,
            BackgroundColor = Color.White,
            StartDay = DayOfWeek.Monday,
            StartDate = DateTime.Now,
            HeightRequest = Device.RuntimePlatform == Device.Android ? 1080 : 720,
        });
        
        diary.Children.Add(listView = new ListView
        {
            ItemTemplate = new DataTemplate(() =>
            {
                StackLayout listItem = new StackLayout
                {
                    Spacing = 15,
                };

                Grid temp = new Grid
                {
                    ColumnDefinitions =
                    {
                        new ColumnDefinition(),
                        new ColumnDefinition()
                    },
                    RowDefinitions =
                    {
                        new RowDefinition(),
                        new RowDefinition()
                    },
                };
                listItem.Children.Add(temp);
                Label nameLabel = new Label
                {
                    TextColor = Color.Black,
                    FontSize = 15,
                    FontAttributes = FontAttributes.Bold
                };
                nameLabel.SetBinding(Label.TextProperty, "Title");
                Label contentLabel = new Label
                {
                    TextColor = Color.Gray,
                    FontSize = 12,
                    FontAttributes = FontAttributes.None
                };
                contentLabel.SetBinding(Label.TextProperty, "Content");

                Label dateBeginLabel = new Label
                {
                    TextColor = Color.Gray,
                    HorizontalOptions = LayoutOptions.End,
                    FontSize = 12,
                    FontAttributes = FontAttributes.Italic
                };
                dateBeginLabel.SetBinding(Label.TextProperty, "DateBegin");
                Label dateEndLabel = new Label
                {
                    TextColor = Color.Gray,
                    HorizontalOptions = LayoutOptions.End,
                    FontSize = 12,
                    FontAttributes = FontAttributes.Italic
                };
                dateEndLabel.SetBinding(Label.TextProperty, "DateEnd");
                Label hourBeginLabel = new Label();
                hourBeginLabel.SetBinding(Label.ClassIdProperty, "TimeBegin");
                Label hourEndLabel = new Label();
                hourEndLabel.SetBinding(Label.ClassIdProperty, "TimeEnd");



                temp.Children.Add(nameLabel, 0, 0);
                temp.Children.Add(contentLabel, 0, 1);
                temp.Children.Add(dateBeginLabel, 1, 0);
                temp.Children.Add(dateEndLabel, 1, 1);
                countRow++;
                ViewCell viewCell = new ViewCell
                {
                    View = listItem,
                };
                viewCell.View.BackgroundColor = Color.White;
                viewCell.ClassId = countRow.ToString();
                MenuItem menuItemEdit = new MenuItem
                {
                    Text = "Modifier"
                };
                menuItemEdit.SetBinding(MenuItem.ClassIdProperty, "ID");
                menuItemEdit.Clicked += OnMenuItemEdit;
                viewCell.ContextActions.Add(menuItemEdit);
                MenuItem menuItemDelete = new MenuItem
                {
                    Text = "Supprimer"
                };
                menuItemDelete.SetBinding(MenuItem.ClassIdProperty, "ID");
                menuItemDelete.Clicked += OnMenuItemDelete;
                viewCell.ContextActions.Add(menuItemDelete);
                viewCell.Appearing += OnRowAppear;
                return viewCell;
            })

        });
        listView.ItemSelected += OnItemSelect;
        cal.DateClicked += CurrentDateClicked;
        this.Content = diary;
    }
    protected override async void OnAppearing()
    {
        base.OnAppearing();
        List<Note> listNote = await App.Database.GetNotesByUserIdAsync(App.userId);
        List<SpecialDate> specialDates = new List<SpecialDate>();
        foreach(Note note in listNote)
        {
            specialDates.Add(new SpecialDate(note.DateBegin)
            {
                Selectable = true,
                BackgroundPattern = new BackgroundPattern(1)
                {
                    Pattern = new List<Pattern>
                        {
                            new Pattern { 
                                WidthPercent = 1f,
                                HightPercent = 0.6f,
                                Color = Color.LightSalmon 
                            },
                            new Pattern {
                                WidthPercent = 1f,
                                HightPercent = 0.4f,
                                Color = Color.LightSalmon,
                                Text = ".",
                                TextColor=Color.CornflowerBlue,
                                TextSize=20,
                                TextAlign=TextAlign.Middle
                            },
                        }
                }
            });
        }
        cal.SpecialDates = specialDates;            
        cal.SelectedDate = DateTime.Now;
    }
    public async void CurrentDateClicked(object sender, EventArgs args)
    {
        countRow = 0;
        DateTime dateSelect = (DateTime)(sender as Calendar).SelectedDate;
        
        listView.ItemsSource = await App.Database.GetNotesFromDate(dateSelect, App.userId);
    }
    async void OnMenuItemEdit(object sender, EventArgs e)
    {
        string data = (sender as MenuItem).ClassId as string;
        //DisplayAlert("Id de la note", data, "Annuler");
        int id = int.Parse(data);
        Note dataNote = await App.Database.GetNoteAsync(id);
        await Navigation.PushAsync(new NoteEditPage(dataNote));
    }
    async void OnMenuItemDelete(object sender, EventArgs e)
    {
        string data = (sender as MenuItem).ClassId as string;
        //DisplayAlert("Id de la note", data, "Annuler");
        int id = int.Parse(data);
        bool answer = await DisplayAlert("Suppression", " Êtes-vous sûr de vouloir supprimer cette note ?", "Oui", "Non");
        if (answer)
        {
            var note = await App.Database.GetNoteAsync(id);
            int result = await App.Database.DeleteNoteAsync(note);
            listView.ItemsSource = await App.Database.GetNotesByUserIdAsync(App.userId);
        }
    }
    async void OnItemSelect(object sender, SelectedItemChangedEventArgs e)
    {
        var item = (Note)e.SelectedItem;
        await Navigation.PushAsync(new NoteEditPage(item));
    }
    void OnRowAppear(object sender, EventArgs e)
    {
        ViewCell view = (ViewCell)sender;
        int rank = int.Parse(view.ClassId);
        if (rank % 2 == 0)
        {
            view.View.BackgroundColor = Color.White;
        }
        else
        {
            view.View.BackgroundColor = Color.LightGray;
        }
    }
    
}
