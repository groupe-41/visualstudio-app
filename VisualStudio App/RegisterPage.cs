﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using VisualStudio_App;
using Xamarin.Forms;

public partial class RegisterPage : ContentPage
{
    Entry pseudoEntry;
    Entry passwordEntry;
    Entry confirmPasswordEntry;
    Label warningLabel;
    Button signInButton;
    Button logInButton;
    ListView listView;
    public RegisterPage()
    {
        NavigationPage.SetHasNavigationBar(this, false);
        this.Padding = new Thickness(30, 30, 30, 30);

        StackLayout panel = new StackLayout
        {
            IsVisible = true,
            VerticalOptions = LayoutOptions.CenterAndExpand,
        };
        panel.Children.Add(pseudoEntry = new Entry
        {
            ReturnType = ReturnType.Next,
            MaxLength = 10,
            Placeholder = "Pseudo",
            FontSize = 15,
            TextColor = Color.FromHex("#000000"),
            Keyboard = Keyboard.Text
        });
        panel.Children.Add(passwordEntry = new Entry
        {
            ReturnType = ReturnType.Next,
            MaxLength = 12,
            IsPassword = true,
            Placeholder = "Mot de passe",
            FontSize = 15,
            TextColor = Color.FromHex("#000000"),
        });panel.Children.Add(confirmPasswordEntry = new Entry
        {
            ReturnType = ReturnType.Next,
            MaxLength = 12,
            IsPassword = true,
            Placeholder = "Confirmer le mot de passe",
            FontSize = 15,
            TextColor = Color.FromHex("#000000"),
        });
        panel.Children.Add(warningLabel = new Label
        {
            IsVisible = false,
            TextColor = Color.Blue,
            FontSize = 15,
            BackgroundColor = Color.Transparent,
        });
        panel.Children.Add(signInButton = new Button
        {
            FontSize = 15,
            TextColor = Color.White,
            BorderColor = Color.Black,
            BackgroundColor = Color.FromHex("#00b33c"),
            CornerRadius = 5,
            Text = "S'inscrire"
        });
        panel.Children.Add(logInButton = new Button
        {
            TextColor = Color.Black,
            BackgroundColor = Color.Transparent,
            Text = "Se connecter",
            FontSize = 13
        });
        /*panel.Children.Add(listView = new ListView
        {
            ItemTemplate = new DataTemplate(() =>
            {
                Label nameLabel = new Label();
                nameLabel.SetBinding(Label.TextProperty, "pseudo");
                Label pswdLabel = new Label();
                pswdLabel.SetBinding(Label.TextProperty, "password");

                ViewCell viewCell = new ViewCell
                {
                    View = new StackLayout
                    {
                        Children =
                        {
                            nameLabel,
                            pswdLabel
                        }
                    }
                };
                return viewCell;
            })
        });*/
        signInButton.Clicked += SignupValidation_ButtonClicked;
        logInButton.Clicked += login_ClickedEvent;

        this.Content = panel;

    }
    protected override async void OnAppearing()
    {
        base.OnAppearing();
        //listView.ItemsSource = await App.Database.GetUsersAsync();
    }
    async void SignupValidation_ButtonClicked(object sender, EventArgs e)
    {
        
        if ((string.IsNullOrWhiteSpace(pseudoEntry.Text)) || (string.IsNullOrWhiteSpace(passwordEntry.Text)))
        {
            await DisplayAlert("Entrez vos informations", "Entrez des informations valides", "OK");
        }
        else if (!string.Equals(passwordEntry.Text, confirmPasswordEntry.Text))
        {
            warningLabel.Text = "Entrez un mot de passe similaire";
            passwordEntry.Text = string.Empty;
            confirmPasswordEntry.Text = string.Empty;
            warningLabel.TextColor = Color.IndianRed;
            warningLabel.IsVisible = true;
        }
        else
        {
            User user = new User
            {
                pseudo = pseudoEntry.Text,
                password = passwordEntry.Text
            };
            bool verif = await App.Database.updateUserValidation(pseudoEntry.Text);
            //await DisplayAlert("Bool", verif.ToString(), "OK");
            if (verif == true)
            {
                warningLabel.Text = "Le pseudo saisie est déjà utilisé";
                passwordEntry.Text = string.Empty;
                confirmPasswordEntry.Text = string.Empty;
                warningLabel.TextColor = Color.IndianRed;
                warningLabel.IsVisible = true;
            }
            else 
            {
                try
                {
                    var result = await App.Database.AddUser(user);
                    if (result == "Ajouté avec succès.")
                    {
                        await DisplayAlert("Inscription", result, "OK");
                        await Navigation.PushAsync(new LoginPage());
                    }
                    else
                    {
                        await DisplayAlert("Inscription", result, "OK");
                        warningLabel.IsVisible = false;
                        pseudoEntry.Text = string.Empty;
                        passwordEntry.Text = string.Empty;
                        confirmPasswordEntry.Text = string.Empty;
                    }
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                
            }
        }
    }

    private async void login_ClickedEvent(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new LoginPage());
    }
}
