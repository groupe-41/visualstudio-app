﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VisualStudio_App;
using Xamarin.Essentials;
using Xamarin.Forms;

public partial class NoteEditPage : ContentPage
{
    Grid gridEntry;
    Entry titleEntry;
    Editor contentEditor;
    DatePicker dateBegin;
    DatePicker dateEnd;
    TimePicker timeBegin;
    TimePicker timeEnd;
    Button saveButton;
    Button deleteButton;
    Label dateStartLabel;
    public NoteEditPage(Note data) 
    { 
        this.Padding = new Thickness(20, 20, 20, 20);
        StackLayout panel = new StackLayout
        {
            Spacing = 15,
            VerticalOptions = LayoutOptions.CenterAndExpand
        };
        panel.Children.Add(titleEntry = new Entry
        {
            Placeholder = "Titre",
            Text = data.Title,
            ClassId = data.ID.ToString()
        }); ;
        panel.Children.Add(contentEditor = new Editor
        {
            Placeholder = "Rentrer votre message",
            Text = data.Content,
            HeightRequest = 200
        });
        panel.Children.Add(gridEntry = new Grid
        {
            RowDefinitions =
        {
            new RowDefinition(),
            new RowDefinition(),
            new RowDefinition(),
            new RowDefinition(),
            new RowDefinition()
        },
            ColumnDefinitions =
        {
            new ColumnDefinition(),
            new ColumnDefinition()
        }
        });
        gridEntry.Children.Add(dateStartLabel = new Label
        {
            Text = "Date de Début"
        }, 0, 0);
        gridEntry.Children.Add(dateStartLabel = new Label
        {
            Text = "Date de Fin"
        }, 1, 0);
        gridEntry.Children.Add(dateBegin = new DatePicker
        {
            MinimumDate = new DateTime(),
            Date = data.DateBegin
        }, 0, 1);
        gridEntry.Children.Add(dateEnd = new DatePicker
        {
            MinimumDate = new DateTime(),
            Date = data.DateEnd
        }, 1, 1);

        gridEntry.Children.Add(dateStartLabel = new Label
        {
            Text = "Heure de Début"
        }, 0, 2);
        gridEntry.Children.Add(dateStartLabel = new Label
        {
            Text = "Heure de Fin"
        }, 1, 2);
        gridEntry.Children.Add(timeBegin = new TimePicker
        {
            Time = data.TimeBegin
        }, 0, 3);
        gridEntry.Children.Add(timeEnd = new TimePicker
        {
            Time = data.TimeEnd
        }, 1, 3);
        gridEntry.Children.Add(saveButton = new Button
        {
            FontSize = 15,
            TextColor = Color.White,
            BorderColor = Color.Black,
            BackgroundColor = Color.Green,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
            WidthRequest = DeviceDisplay.MainDisplayInfo.Width,
            CornerRadius = 5,
            Text = "Enregistrer"
        }, 0, 4);
        gridEntry.Children.Add(deleteButton = new Button
        {
            FontSize = 15,
            TextColor = Color.White,
            BorderColor = Color.Black,
            BackgroundColor = Color.Red,
            HorizontalOptions = LayoutOptions.CenterAndExpand,
            WidthRequest = DeviceDisplay.MainDisplayInfo.Width,
            CornerRadius = 5,
            Text = "Supprimer"
        }, 1, 4);
        dateBegin.DateSelected += OnDateBeginSelected;
        saveButton.Clicked += OnSaveButtonClicked;
        deleteButton.Clicked += OnDeleteButtonClicked;

        this.Content = panel;

    }


    async void OnSaveButtonClicked(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(titleEntry.Text) && !string.IsNullOrWhiteSpace(contentEditor.Text))
        {
            TimeSpan BeginTime = new TimeSpan(timeBegin.Time.Hours, timeBegin.Time.Minutes, timeBegin.Time.Seconds);
            TimeSpan EndTime = new TimeSpan(timeEnd.Time.Hours, timeEnd.Time.Minutes, timeEnd.Time.Seconds);

            if (dateBegin.Date == dateEnd.Date && BeginTime >= EndTime)
            {
                TimeSpan temp = EndTime;
                EndTime = BeginTime;
                BeginTime = temp;
            }

            await App.Database.UpdateNoteAsync(new Note
            {
                ID = int.Parse(titleEntry.ClassId),
                Title = titleEntry.Text,
                Content = contentEditor.Text,
                DateBegin = new DateTime(dateBegin.Date.Year, dateBegin.Date.Month, dateBegin.Date.Day, BeginTime.Hours, BeginTime.Minutes, 0),
                DateEnd = new DateTime(dateEnd.Date.Year, dateEnd.Date.Month, dateEnd.Date.Day, EndTime.Hours, EndTime.Minutes, 0),
                TimeBegin = BeginTime,
                TimeEnd = EndTime,
                UserId = App.userId
            }); ; ;

            titleEntry.Text = contentEditor.Text = string.Empty;
            await Navigation.PopAsync();
        }
    }

    async void OnDeleteButtonClicked(object sender, EventArgs e)
    {
        bool answer = await DisplayAlert("Suppression", " Êtes-vous sûr de vouloir supprimer cette note ?", "Oui", "Non");
        if (answer)
        {
            var note = await App.Database.GetNoteAsync(int.Parse(titleEntry.ClassId));
            int result = await App.Database.DeleteNoteAsync(note);
            await Navigation.PopAsync();
        }
    }

    void OnDateBeginSelected(object sender, DateChangedEventArgs e)
    {
        var item = (DateTime)e.NewDate;
        dateEnd.MinimumDate = item;
    }

}
