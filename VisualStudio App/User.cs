﻿using SQLite;

namespace VisualStudio_App

{

    public class User

    {

        [PrimaryKey, AutoIncrement]

        public int Id { get; set; }

        public string pseudo { get; set; }

        public string password { get; set; }

        public User()

        {

        }

    }

}