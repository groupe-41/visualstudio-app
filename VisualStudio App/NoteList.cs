﻿using System;
using System.Collections.Generic;
using System.Text;
using VisualStudio_App;
using Xamarin.Forms;

public partial class NoteList : ContentPage
{
    Button addButton;
    Button calendarButton;
    ListView listView;
    Grid buttonGrid;
    int countRow = 0;
    public NoteList()
    {
        NavigationPage.SetHasBackButton(this, false);
        ToolbarItems.Add(new ToolbarItem("Deconnexion", "iconssignout24.png", logOutAction));
        this.Padding = new Thickness(0, 20, 0, 20);

        StackLayout panel = new StackLayout
        {
            Spacing = 15,

        };
        Image addImage = new Image
        {
            Source = "iconsadd.png"
        };
        panel.Children.Add(buttonGrid = new Grid
        {
            ColumnDefinitions = { new ColumnDefinition(), new ColumnDefinition() }
        });
        buttonGrid.Children.Add(addButton = new Button
        {
            ImageSource = ImageSource.FromFile("iconsadd24.png"),         
            WidthRequest = 60,
            HeightRequest = 60,
            CornerRadius = 30,
            HorizontalOptions = LayoutOptions.Center,
            BackgroundColor = Color.CornflowerBlue
        },0,0);
        buttonGrid.Children.Add(calendarButton = new Button
        {
            ImageSource = ImageSource.FromFile("iconscalendar26.png"),
            WidthRequest = 60,
            HeightRequest = 60,
            CornerRadius = 30,
            HorizontalOptions = LayoutOptions.Center,
            BackgroundColor = Color.CornflowerBlue
        },1,0);
        panel.Children.Add(listView = new ListView
        {
            ItemTemplate = new DataTemplate(() =>
            {             
                StackLayout listItem = new StackLayout
                {
                    Spacing = 15,
                };

                Grid temp = new Grid
                {
                    ColumnDefinitions =
                    {
                        new ColumnDefinition(),
                        new ColumnDefinition()
                    },
                    RowDefinitions =
                    {
                        new RowDefinition(),
                        new RowDefinition()
                    },
                };
                listItem.Children.Add(temp);
                Label nameLabel = new Label
                {
                    TextColor = Color.Black,
                    FontSize = 15,
                    FontAttributes = FontAttributes.Bold
                };
                nameLabel.SetBinding(Label.TextProperty, "Title");
                Label contentLabel = new Label
                {
                    TextColor = Color.Gray,
                    FontSize = 12,
                    FontAttributes = FontAttributes.None
                };
                contentLabel.SetBinding(Label.TextProperty, "Content");
               
                Label dateBeginLabel = new Label
                { 
                    TextColor = Color.Gray,
                    HorizontalOptions = LayoutOptions.End,
                    FontSize = 12,
                    FontAttributes = FontAttributes.Italic
                };
                dateBeginLabel.SetBinding(Label.TextProperty, "DateBegin");
                Label dateEndLabel = new Label
                {
                    TextColor = Color.Gray,
                    HorizontalOptions = LayoutOptions.End,
                    FontSize = 12,
                    FontAttributes = FontAttributes.Italic
                };
                dateEndLabel.SetBinding(Label.TextProperty, "DateEnd");
                Label hourBeginLabel = new Label();
                hourBeginLabel.SetBinding(Label.ClassIdProperty, "TimeBegin");
                Label hourEndLabel = new Label();
                hourEndLabel.SetBinding(Label.ClassIdProperty, "TimeEnd");

               

                temp.Children.Add(nameLabel,0,0);
                temp.Children.Add(contentLabel,0,1);
                temp.Children.Add(dateBeginLabel,1,0);
                temp.Children.Add(dateEndLabel,1,1);
                countRow++;
                ViewCell viewCell = new ViewCell
                {
                    View = listItem,
                };
                viewCell.View.BackgroundColor = Color.White;
                viewCell.ClassId = countRow.ToString();
                MenuItem menuItemEdit = new MenuItem
                {
                    Text = "Modifier"
                };
                menuItemEdit.SetBinding(MenuItem.ClassIdProperty, "ID");
                menuItemEdit.Clicked += OnMenuItemEdit;
                viewCell.ContextActions.Add(menuItemEdit);
                MenuItem menuItemDelete = new MenuItem
                {
                    Text = "Supprimer"
                };
                menuItemDelete.SetBinding(MenuItem.ClassIdProperty, "ID");
                menuItemDelete.Clicked += OnMenuItemDelete;
                viewCell.Appearing += OnRowAppear;
                viewCell.ContextActions.Add(menuItemDelete);
                return viewCell;
            })
        });
        listView.ItemSelected += OnItemSelect;
        
        calendarButton.Clicked += NavigateCalendar;
        addButton.Clicked += OnClickButton; 
        this.Content = panel;
    }

    protected override async void OnAppearing()
    {
        base.OnAppearing();
        listView.ItemsSource = await App.Database.GetNotesByUserIdAsync(App.userId);
    }
    
    async void OnClickButton(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new NoteEntryPage());
    }
    async void OnMenuItemEdit(object sender, EventArgs e)
    {
        string data = (sender as MenuItem).ClassId as string;
        //DisplayAlert("Id de la note", data, "Annuler");
        int id = int.Parse(data);
        Note dataNote = await App.Database.GetNoteAsync(id);
        await Navigation.PushAsync(new NoteEditPage(dataNote));
    }
    async void OnMenuItemDelete(object sender, EventArgs e)
    {
        string data = (sender as MenuItem).ClassId as string;
        //DisplayAlert("Id de la note", data, "Annuler");
        int id = int.Parse(data);
        bool answer = await DisplayAlert("Suppression", " Êtes-vous sûr de vouloir supprimer cette note ?", "Oui", "Non");
        if (answer)
        {
            var note = await App.Database.GetNoteAsync(id);
            int result = await App.Database.DeleteNoteAsync(note);
            listView.ItemsSource = await App.Database.GetNotesByUserIdAsync(App.userId);
        }
    }
    async void NavigateCalendar(object sender, EventArgs e)
    {        
        await Navigation.PushAsync(new Calendar_App());
    }
    async void OnItemSelect(object sender, SelectedItemChangedEventArgs e)
    {
        var item = (Note) e.SelectedItem;
        await Navigation.PushAsync(new NoteEditPage(item));
    }
    async void logOutAction()
    {
        await Navigation.PopToRootAsync();
    }    
    void OnRowAppear(object sender, EventArgs e)
    {
        ViewCell view = (ViewCell)sender;
        int rank = int.Parse(view.ClassId);
        if (rank % 2 == 0)
        {
            view.View.BackgroundColor = Color.White;
        }
        else
        {
            view.View.BackgroundColor = Color.LightGray;
        }
    }
}
 